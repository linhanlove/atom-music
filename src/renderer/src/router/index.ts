// src\router\index.ts
import { createRouter, createWebHashHistory } from 'vue-router'

const router = createRouter({
  //  hash 模式。
  history: createWebHashHistory(),
  routes: [
    // 设置首页
    {
      path: '/',
      component: () => import('@renderer/views/layout/index.vue')
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('@renderer/views/Home/index.vue')
    },
  ],
})



export default router
